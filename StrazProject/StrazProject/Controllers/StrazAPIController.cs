﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StrazProject.DAL;

namespace StrazProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StrazAPIController : ControllerBase
    {
		private readonly IFiremanData _db;

		public StrazAPIController(IFiremanData db)
		{
			_db = db;
		}

        // GET: api/StrazAPI
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/StrazAPI/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/StrazAPI
        [HttpPost]
        public void Post([FromBody] string value)
        {
			
		}
    }
}
