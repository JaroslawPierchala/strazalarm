﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using StrazProject.DAL;
using StrazProject.Models;
using StrazProject.ViewModels;

namespace StrazProject.Controllers
{
	public class StrazAlarmController : Controller
	{
		public static List<Fireman> FiremanList = new List<Fireman>();
		public static List<FireAction> FireActions = new List<FireAction>();
		public static List<FireBrigade> FireBrigades = new List<FireBrigade>();

		private readonly IMapper _mapper;
		private readonly IFiremanData _db;

		public StrazAlarmController(IMapper mapper, IFiremanData db)
		{
			_mapper = mapper;
			_db = db;
		}
		public IActionResult Index()
		{
			FiremanList = _db.GetAllFireman().ToList();
			FireActions = _db.GetAllFireActions().ToList();
			MainPageViewModel fireFighters = new MainPageViewModel() { FiremanList = FiremanList, FireActions = FireActions };
			return View(fireFighters);
		}
		public IActionResult CreateFireman()
		{
			FireBrigades = _db.GetAllFireBrigades().ToList();
			ViewBag.FireBrigadeNames = FireBrigades.Select(x => new SelectListItem { Text = x.FireBrigadeName, Value = x.FireBrigadeId.ToString() });
			return View();
		}
		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult CreateFireman(FiremanCreateViewModel fireman)
		{
			try
			{
				if (ModelState.IsValid)
				{
					var mappedFireman = _mapper.Map<FiremanCreateViewModel, Fireman>(fireman);
					mappedFireman.SignalRId = "";
					_db.InsertFireman(mappedFireman);

					return RedirectToAction(nameof(CreateFireman));
				}
				return View(fireman);
			}
			catch (Exception ex)
			{
				return View();
			}
		}
		public IActionResult ActionHub(int fireActionId, string fireBrigadeId)
		{
			List<FiremanFireAction> firemanFireAction = _db.GetAllFiremanFireActions()
				.Where(x => x.FireActionId == fireActionId).ToList();
			List<FireActionHubViewModel> actionFiremanList = _mapper
				.Map<List<FiremanFireAction>, List<FireActionHubViewModel>>(firemanFireAction);
			if (actionFiremanList.Count == 0)
			{
				FireAction fireAction = _db.GetFireActionById(fireActionId);
				actionFiremanList
					.Add(new FireActionHubViewModel { Action = fireAction,
						Fireman = new Fireman { FireBrigade = _db.GetFireBrigadeById(int.Parse(fireBrigadeId))}});
			}
			return View(actionFiremanList);
		}
		public IActionResult StartFireAction()
		{
			FireBrigades = _db.GetAllFireBrigades().ToList();
			ViewBag.FireBrigadeNames = FireBrigades.Select(x => new SelectListItem { Text = x.FireBrigadeName, Value = x.FireBrigadeId.ToString() });
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult StartFireAction(FireActionStartViewModel fireAction)
		{
			try
			{
				if (ModelState.IsValid)
				{
					var mappedFireAction = _mapper.Map<FireActionStartViewModel, FireAction>(fireAction);
					_db.InsertFireAction(mappedFireAction);

					return RedirectToAction(nameof(ActionHub), new { mappedFireAction.FireActionId, fireAction.FireBrigadeId});
				}
				return View(fireAction);
			}
			catch (Exception ex)
			{
				return View();
			}
		}

		public IActionResult CreateFireBrigade()
		{
			return View();
		}

		[HttpPost]
		[ValidateAntiForgeryToken]
		public IActionResult CreateFireBrigade(FireBrigadeCreateViewModel fireBrigade)
		{
			try
			{
				if (ModelState.IsValid)
				{
					var mappedFireBrigade = _mapper.Map<FireBrigadeCreateViewModel, FireBrigade>(fireBrigade);
					_db.InsertFireBrigade(mappedFireBrigade);

					return RedirectToAction(nameof(CreateFireBrigade));
				}
				return View(fireBrigade);
			}
			catch (Exception ex)
			{
				return View();
			}
		}
	}
}