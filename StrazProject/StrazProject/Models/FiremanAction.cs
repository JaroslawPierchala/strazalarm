﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StrazProject.Models
{
	public class FiremanFireAction
	{
		public int FiremanId { get; set; }
		public int FireActionId { get; set; }

		public Fireman Fireman { get; set; }
		public FireAction FireAction { get; set; }

		public string EstimatedTimeOfArrival { get; set; }
		public bool Participation { get; set; }
	}
}
