﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StrazProject.Models
{
	public class Fireman
	{
		public int FiremanId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string PhoneNumber { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public string SignalRId { get; set; }

		public int FireBrigadeId { get; set; }
		public FireBrigade FireBrigade { get; set; }

		public virtual ICollection<FiremanFireAction> FiremanFireAction { get; set; }

		public Fireman()
		{
			this.FiremanFireAction = new HashSet<FiremanFireAction>();
		}
	}
}
