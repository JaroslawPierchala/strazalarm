﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StrazProject.Models
{
	public class FireBrigade
	{
		public int FireBrigadeId { get; set; }
		public string FireBrigadeName { get; set; }

		public virtual ICollection<Fireman> Fireman { get; set; }

		public FireBrigade()
		{
			this.Fireman = new HashSet<Fireman>();
		}
	}
}
