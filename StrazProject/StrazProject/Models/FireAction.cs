﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StrazProject.Models
{
	public class FireAction
	{
		public int FireActionId { get; set; }
		public string FireActionName { get; set; }
		public DateTime FireActionDate { get; set; }

		public virtual ICollection<FiremanFireAction> FiremanFireAction { get; set; }

		public FireAction()
		{
			this.FiremanFireAction = new HashSet<FiremanFireAction>();
		}
	}
}
