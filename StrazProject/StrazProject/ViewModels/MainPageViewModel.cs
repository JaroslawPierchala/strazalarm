﻿using StrazProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StrazProject.ViewModels
{
	public class MainPageViewModel
	{
		public List<FireAction> FireActions{ get; set; }
		public List<Fireman> FiremanList { get; set; }
	}
}
