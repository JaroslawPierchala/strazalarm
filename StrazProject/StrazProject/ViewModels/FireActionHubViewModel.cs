﻿using StrazProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StrazProject.ViewModels
{
	public class FireActionHubViewModel
	{
		public Fireman Fireman { get; set; }
		public FireAction Action { get; set; }

		public string EstimatedTimeOfArrival { get; set; }
		public bool Participation { get; set; }
	}
}
