﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace StrazProject.ViewModels
{
	public class FireActionStartViewModel
	{
		[Required]
		public string FireActionName { get; set; }
		[Required]
		public string FireBrigadeId { get; set; }
	}
}
