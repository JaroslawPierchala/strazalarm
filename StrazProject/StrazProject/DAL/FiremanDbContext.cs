﻿using Microsoft.EntityFrameworkCore;
using StrazProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StrazProject.DAL
{
	public class FiremanDbContext : DbContext
	{
		public DbSet<Fireman> Fireman { get; set; }
		public DbSet<FireAction> FireAction { get; set; }
		public DbSet<FiremanFireAction> FiremanFireAction { get; set; }
		public DbSet<FireBrigade> FireBrigade { get; set; }

		//protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		//{
		//	var connStr = @"Data Source=USER-KOMPUTER\SQLEXPRESS;Database=FiremanDb;Integrated Security=True";

		//	optionsBuilder.UseSqlServer(connStr);
		//}

		public FiremanDbContext(DbContextOptions<FiremanDbContext> options) : base(options) { }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<FiremanFireAction>().HasKey(sc => new { sc.FiremanId, sc.FireActionId });
		}
	}
}
