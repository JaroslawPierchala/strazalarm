﻿using StrazProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StrazProject.DAL
{
	public interface IFiremanData
	{
		IEnumerable<Fireman> GetAllFireman();
		IEnumerable<FireAction> GetAllFireActions();
		IEnumerable<FiremanFireAction> GetAllFiremanFireActions();
		IEnumerable<FireBrigade> GetAllFireBrigades();
		Fireman GetFiremanByLogin(string login);
		FireAction GetFireActionByName(string name);
		IEnumerable<Fireman> GetFiremanListInFireBrigade(string fireBrigadeName);
		FireBrigade GetFireBrigadeById(int fireBrigadeId);
		FireAction GetFireActionById(int fireActionId);
		void InsertFireman(Fireman fireman);
		void InsertFireAction(FireAction fireAction);
		void InsertFiremanFireAction(FiremanFireAction firemanFireAction);
		void InsertFireBrigade(FireBrigade fireBrigade);
		void UpdateFireman(Fireman fireman);
		void UpdateFireAction(FireAction fireAction);
		void UpdateFiremanFireAction(FiremanFireAction firemanFireAction);
		void UpdateFireBrigade(FireBrigade fireBrigade);
		void DeleteFireman(Fireman fireman);
		void DeleteFireAction(FireAction fireAction);
		void DeleteFiremanFireAction(FiremanFireAction firemanFireAction);
		void DeleteFireBrigade(FireBrigade fireBrigade);
	}
}
