﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using StrazProject.Models;

namespace StrazProject.DAL
{
	public class DbFiremanData: IFiremanData
	{
		private readonly FiremanDbContext _context;

		public DbFiremanData(FiremanDbContext context)
		{
			_context = context;
			_context.Database.EnsureCreated();
		}

		public IEnumerable<FireAction> GetAllFireActions()
		{
			return _context.FireAction.ToList();
		}

		public IEnumerable<Fireman> GetAllFireman()
		{
			return _context.Fireman.ToList();
		}

		public IEnumerable<FiremanFireAction> GetAllFiremanFireActions()
		{
			return _context.FiremanFireAction.ToList();
		}

		public IEnumerable<FireBrigade> GetAllFireBrigades()
		{
			return _context.FireBrigade.ToList();
		}

		public void InsertFireAction(FireAction fireAction)
		{
			_context.FireAction.Add(fireAction);
			_context.SaveChanges();
		}

		public void InsertFireman(Fireman fireman)
		{
			_context.Fireman.Add(fireman);
			_context.SaveChanges();
		}

		public void InsertFiremanFireAction(FiremanFireAction firemanFireAction)
		{
			_context.FiremanFireAction.Add(firemanFireAction);
			_context.SaveChanges();
		}

		public void InsertFireBrigade(FireBrigade fireBrigade)
		{
			_context.FireBrigade.Add(fireBrigade);
			_context.SaveChanges();
		}

		public void UpdateFireAction(FireAction fireAction)
		{
			_context.FireAction.Update(fireAction);
			_context.SaveChanges();
		}

		public void UpdateFireman(Fireman fireman)
		{
			_context.Fireman.Update(fireman);
			_context.SaveChanges();
		}

		public void UpdateFiremanFireAction(FiremanFireAction firemanFireAction)
		{
			_context.FiremanFireAction.Update(firemanFireAction);
			_context.SaveChanges();
		}

		public void UpdateFireBrigade(FireBrigade fireBrigade)
		{
			_context.FireBrigade.Update(fireBrigade);
			_context.SaveChanges();
		}

		public void DeleteFireAction(FireAction fireAction)
		{
			_context.FireAction.Remove(fireAction);
			_context.SaveChanges();
		}

		public void DeleteFireman(Fireman fireman)
		{
			_context.Fireman.Remove(fireman);
			_context.SaveChanges();
		}

		public void DeleteFiremanFireAction(FiremanFireAction firemanFireAction)
		{
			_context.FiremanFireAction.Remove(firemanFireAction);
			_context.SaveChanges();
		}

		public void DeleteFireBrigade(FireBrigade fireBrigade)
		{
			_context.FireBrigade.Remove(fireBrigade);
			_context.SaveChanges();
		}

		public Fireman GetFiremanByLogin(string login)
		{
			return _context.Fireman.Where(x => x.Login == login).FirstOrDefault();
		}

		public FireAction GetFireActionByName(string name)
		{
			return _context.FireAction.Where(x => x.FireActionName == name).FirstOrDefault();
		}

		public IEnumerable<Fireman> GetFiremanListInFireBrigade(string fireBrigadeName)
		{
			return _context.Fireman.Where(fire => fire.FireBrigade.FireBrigadeName == fireBrigadeName).ToList();
		}

		public FireBrigade GetFireBrigadeById(int fireBrigadeId)
		{
			return _context.FireBrigade.Where(x => x.FireBrigadeId == fireBrigadeId).FirstOrDefault();
		}

		public FireAction GetFireActionById(int fireActionId)
		{
			return _context.FireAction.Where(x => x.FireActionId == fireActionId).FirstOrDefault();
		}
	}
}
