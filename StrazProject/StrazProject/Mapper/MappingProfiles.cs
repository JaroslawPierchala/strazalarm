﻿using AutoMapper;
using StrazProject.Models;
using StrazProject.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StrazProject.Mapper
{
	public class MappingProfiles : Profile
	{
		public MappingProfiles()
		{
			CreateMap<FiremanCreateViewModel, Fireman>();
			CreateMap<FiremanFireAction, FireActionHubViewModel>()
				.ForMember(desc => desc.EstimatedTimeOfArrival, opt => opt.MapFrom(src => src.EstimatedTimeOfArrival));
			CreateMap<FireActionStartViewModel, FireAction>()
				.ForMember(desc => desc.FireActionDate, opt => opt.MapFrom(src => DateTime.Now));
			CreateMap<FireBrigadeCreateViewModel, FireBrigade>();
		}
	}
}
