﻿using Microsoft.AspNetCore.SignalR;
using StrazProject.DAL;
using StrazProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StrazProject.Services
{
	public class SignalRFiremanHub : Hub
	{
		private readonly IFiremanData _db;

		public SignalRFiremanHub(IFiremanData db)
		{
			_db = db;
		}
		public async Task SendActionAlarm(string fireActionName, string fireBrigadeName)
		{
			await Clients
				.Clients
				(_db.GetFiremanListInFireBrigade(fireBrigadeName)
					.Select(x => x.SignalRId)
					.ToList())
				.SendAsync("ReceiveFireActionAlarm", _db.GetFireActionByName(fireActionName).FireActionId, fireActionName);
			//await Clients.Others.SendAsync("ReceiveFireActionAlarm", _db.GetFireActionByName(fireActionName).FireActionId, fireActionName);
		}

		public async Task SendFireman(string login)
		{
			Fireman fireman = _db.GetFiremanByLogin(login);
			fireman.SignalRId = Context.ConnectionId;
			_db.UpdateFireman(fireman);
			//_db.InsertFiremanFireAction(new FiremanFireAction
			//{
			//	FiremanId = _db.GetFiremanByLogin(login).FiremanId,
			//	EstimatedTimeOfArrival = estimatedTimeOfArrival,
			//	Participation = participation
			//});
		}
		public async Task SendActionAnswer(int fireActionId, string login, string estimatedTimeOfArrival, bool participation)
		{
			_db.InsertFiremanFireAction(new FiremanFireAction
			{
				FiremanId = _db.GetFiremanByLogin(login).FiremanId,
				FireActionId = fireActionId,
				EstimatedTimeOfArrival = estimatedTimeOfArrival,
				Participation = participation
			});
			await Clients.Others.SendAsync("ReceiveConfirmation");
		}
	}
}
