﻿using Nexmo.Api.Voice;
using StrazProject.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace StrazProject.Services
{
	public interface INexmoService
	{
		void MakeCall(string to);
		Task<List<FiremanFireAction>> CallFireman(List<Fireman> firemanList, FireAction fireAction);
		void HangupCall(string uuid);
		bool CheckStatus(Call.CallResponse call);
	}
}