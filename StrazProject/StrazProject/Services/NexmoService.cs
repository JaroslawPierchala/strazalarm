﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Nexmo.Api;
using Nexmo.Api.Voice;
using StrazProject.Models;

namespace StrazProject.Services
{
	public class NexmoService : INexmoService
	{
		private readonly IHostingEnvironment _host;
		private readonly Client _client;
		private readonly List<Call.CallResponse> _calls;
		public NexmoService(IHostingEnvironment host)
		{
			_host = host;
			var path = Path.Combine(_host.WebRootPath, "nexmo", "private.key");
			_client = new Client(creds: new Nexmo.Api.Request.Credentials
			{
				ApiKey = "f4e5c7f3",
				ApiSecret = "9SrsGssZbecOaLdG",
				ApplicationId = "6409c831-5df8-41d7-b6f2-1a172150b0a5",
				ApplicationKey = path
			});
		}

		public async Task<List<FiremanFireAction>> CallFireman(List<Fireman> firemanList, FireAction fireAction)
		{
			List<FiremanFireAction> result = new List<FiremanFireAction>();
			List<Task> tasks = new List<Task>();
			foreach (var fireman in firemanList)
			{
				tasks.Add(Task.Run(() => MakeCall(fireman.PhoneNumber)));
			}

			await Task.WhenAll(tasks);

			foreach (var call in _calls)
			{
				result.Add(Task.Run(() => 
				{
					var firemanFireAction = new FiremanFireAction
					{
						FiremanId = firemanList
							.Where(x => x.PhoneNumber == call.to.number)
							.Select(x => x.FiremanId)
							.FirstOrDefault(),
						Participation = CheckStatus(call),
						FireActionId = fireAction.FireActionId
					};
					return firemanFireAction;
				}).Result);
			}
			return result;
		}

		public bool CheckStatus(Call.CallResponse call)
		{
			switch (call.status)
			{
				case "started":
					{
						Thread.Sleep(3000);
						return CheckStatus(call);
					};
				case "ringing":
					{
						Thread.Sleep(3000);
						return CheckStatus(call);
					};
				case "answered":
					{
						HangupCall(call.uuid);
						return true;
					};
				case "completed": return true;
				case "rejected": return false;
				case "timeout": return false;
				case "unanswered": return false;
			}
			return false;
		}

		public void MakeCall(string to)
		{
			var TO_NUMBER = to;
			var NEXMO_NUMBER = "NEXMO_NUMBER";

			var results = _client.Call.Do(new Call.CallCommand
			{
				to = new[]
				{
					new Call.Endpoint {
						type = "phone",
						number = TO_NUMBER
					}
				},
				from = new Call.Endpoint
				{
					type = "phone",
					number = NEXMO_NUMBER
				}
			});
			_calls.Add(results);

			//Session["UUID"] = results.uuid;
		}

		public void HangupCall(string uuid)
		{
			var result = _client.Call.Edit(uuid, new Call.CallEditCommand
			{
				Action = "hangup"
			});
		}
	}
}
